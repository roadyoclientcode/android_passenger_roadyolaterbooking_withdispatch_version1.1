package com.roadyo.passenger.main;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.threembed.roadyo.R;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;

public class InviteFragment extends Fragment implements OnClickListener
{
	private  View view;
	private String emailOrMessage[]={"Email","Message"};
	private ImageView facebook_share,twitter_share,message_share,email_share;
	private TextView coupon_code;
	private SessionManager session;
	ShareDialog shareDialog;
	CallbackManager callbackManager;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		if(view != null) 
		{
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}

		try 
		{
			view = inflater.inflate(R.layout.invite_screen, container, false);
			initialize();

		} catch (InflateException e)
		{
			Utility.printLog("onCreateView  InflateException "+e);
		}
		
		session = new SessionManager(getActivity());
		coupon_code.setText(session.getCouponCode());


		Utility.printLog("coupon code"+session.getCouponCode());
		if(Utility.isNetworkAvailable(getActivity()))
		{
		}
		else
		{
			Intent homeIntent=new Intent("com.threembed.roadyo.internetStatus");
			homeIntent.putExtra("STATUS", "0");
			getActivity().sendBroadcast(homeIntent);
		}




		return view;
	}

	private void initialize()
	{
		facebook_share = (ImageView)view.findViewById(R.id.facebook_share);
		twitter_share = (ImageView)view.findViewById(R.id.twitter_share);
		message_share = (ImageView)view.findViewById(R.id.message_share);
		email_share = (ImageView)view.findViewById(R.id.email_share);
		coupon_code = (TextView)view.findViewById(R.id.share_code);

		FacebookSdk.sdkInitialize(getActivity());
		callbackManager = CallbackManager.Factory.create();
		shareDialog = new ShareDialog(this);


		facebook_share.setOnClickListener(this);
		twitter_share.setOnClickListener(this);
		message_share.setOnClickListener(this);
		email_share.setOnClickListener(this);

		MainActivityDrawer.driver_tip.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId() ==  R.id.facebook_share)
		{
			/*String urlToShare = "https://www.facebook.com/roadyo";
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, urlToShare);
			boolean facebookAppFound = false;
			List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
			for (ResolveInfo info : matches) {
				if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
					intent.setPackage(info.activityInfo.packageName);
					facebookAppFound = true;
					break;
				}
			}

			if(facebookAppFound)
			{
				startActivity(intent);
			}
			else
			{
				Intent shareIntent = new Intent();
				shareIntent.setAction(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello,"+"\n"+"  "+"Use my  referral code,"+" "+session.getCouponCode()+" "+"to signup on"+" "+getResources().getString(R.string.app_name)
						+" "+"and you will  get an exclusive promo code via email. Go to link https://play.google.com/store/apps/details?id=com.threembed.roadyo"
						);
				startActivity(Intent.createChooser(shareIntent, "Share your promo code"));
			}*/

			String text = "Register on Roadyo with " +session.getCouponCode()+ " and earn discounts on your first ride." ;

			if (ShareDialog.canShow(ShareLinkContent.class))
			{
				ShareLinkContent linkContent = new ShareLinkContent.Builder()
						.setContentTitle(getResources().getString(R.string.app_name))
						.setContentDescription(text)
						.setImageUrl(Uri.parse("http://www.roadyo.in/roadyo1.0/appimages/roadyo_passanger_icon.png"))
						.setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.threembed.roadyo"))
						.build();

				shareDialog.show(linkContent);

			}

		}
		else if(v.getId() ==  R.id.twitter_share)
		{
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, "Hello,"+"\n"+"  "+"Use my  referral code,"+" "+session.getCouponCode()+" "+"to signup on"+" "+getResources().getString(R.string.app_name)
					+" "+"and you will  get an exclusive promo code via email. Go to link https://play.google.com/store/apps/details?id=com.threembed.roadyo"
					);
			boolean facebookAppFound = false;
			List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
			for (ResolveInfo info : matches) {
				if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
					intent.setPackage(info.activityInfo.packageName);
					facebookAppFound = true;
					break;
				}
			}

			if(facebookAppFound)
			{
				startActivity(intent);
			}
			else
			{
				Intent shareIntent = new Intent();
				shareIntent.setAction(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello,"+"\n"+"  "+"Use my  referral code,"+" "+session.getCouponCode()+" "+"to signup on"+" "+getResources().getString(R.string.app_name)
						+" "+"and you will  get an exclusive promo code via email."
						);
				startActivity(Intent.createChooser(shareIntent, "Share your promo code"));
			}

		}
		else if(v.getId() ==  R.id.message_share)
		{

			String smsBody="Hello,"+"\n"+"  "+"Use my  referral code,"+" "+session.getCouponCode()+" "+"to signup on"+" "+getResources().getString(R.string.app_name)
					+" "+"and you will  get an exclusive promo code via email. Go to link https://play.google.com/store/apps/details?id=com.threembed.roadyo";

			Intent intentsms = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:"));
			intentsms.putExtra( "sms_body", smsBody);
			startActivity( intentsms );
		}
		else if(v.getId() ==  R.id.email_share)
		{
			Intent email = new Intent(Intent.ACTION_SEND);
			//email.putExtra(Intent.EXTRA_EMAIL, new String[]{"youremail@yahoo.com"});		  
			email.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.registeron)+" " +getResources().getString(R.string.app_name));
			email.putExtra(Intent.EXTRA_TEXT, "Hello,"+"\n"+"  "+"Use my  referral code,"+" "+session.getCouponCode()+" "+"to signup on"+" "+getResources().getString(R.string.app_name)
					+" "+"and you will  get an exclusive promo code via email. Go to link https://play.google.com/store/apps/details?id=com.threembed.roadyo"
					);
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email, "Choose an Email client :"));
		}
	}


	private void selectOptionMenu(final String phNumb,final String email_addr) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Select");

		builder.setItems(emailOrMessage, new DialogInterface.OnClickListener() 
		{

			@Override
			public void onClick(DialogInterface optiondialog, int which) 
			{
				if (emailOrMessage[which].equals(emailOrMessage[0])) 
				{
					Intent emailIntent = new Intent(Intent.ACTION_VIEW);
					Uri data = Uri.parse("mailto:?subject="
							+ "Invite to join PrevPatient App!"
							+ "&body="
							+ "Please download the PocketCab App from www.PocketCab.us"
							+ "&to=" 
							+ email_addr);

					emailIntent.setData(data);
					startActivity(emailIntent);
					optiondialog.dismiss();
				}
				if (emailOrMessage[which].equals(emailOrMessage[1])) 
				{
					/*startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:Please download the SneekPeek App from www.sneekpeek.com"
	                        + phNumb)));*/

					/*Intent smsIntent = new Intent(Intent.ACTION_VIEW);

			        smsIntent.putExtra("sms_body", "Please download the PrevPatient App from www.prevpatient.com"); 
			        smsIntent.putExtra("address", phNumb);
			        smsIntent.setType("vnd.android-dir/mms-sms");

			        startActivity(smsIntent);
			    	optiondialog.dismiss();*/


					Intent intentsms = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + ""+phNumb ) );
					intentsms.putExtra( "sms_body", "Please download the PrevPatient App from www.PocketCab.us" );
					startActivity( intentsms );
					optiondialog.dismiss();
				}
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}
		});
		AlertDialog  alert = builder.create();
		alert.setCancelable(true);
		alert.show();

	}







}
