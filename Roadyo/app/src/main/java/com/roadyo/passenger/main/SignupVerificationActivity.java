package com.roadyo.passenger.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.UploadImgeResponse;
import com.roadyo.passenger.pojo.ValidVerificationCodeResponse;
import com.roadyo.passenger.pojo.VerificationCodeResponse;
import com.threembed.roadyo.R;
import com.threembed.utilities.ReadSms;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignupVerificationActivity extends Activity
{
	TextView verificationnumber_tv,verificationnumber_countrycode_tv;
	Button resend_verificationcode_bt,signup_back;
	EditText et_verificationcode;
	String regid,deviceid,firstName,lastName,email,password,mobileNo,referal;
	Button signup_payment_skip;
	SessionManager sessionManager;
	public static File mFileTemp;
	String lat,longitude;
	boolean picSelected;
	private ReadSms readSms;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.signup_verification_activity);
		Bundle bundle=getIntent().getExtras();
		//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
		{
			mFileTemp = new File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}
		else
		{
			mFileTemp = new File(getFilesDir(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}



		readSms = new ReadSms() {
			@Override
			protected void onSmsReceived(String str) {
				//Call the verify SMS code API

				/**
				 * to filter the OTP from last 5 numbers
				 */
				String substring = str.substring(Math.max(str.length() - 6, 0));

				et_verificationcode.setText(substring);
			}
		};
		IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		intentFilter.setPriority(1000);
		this.registerReceiver(readSms, intentFilter);

		if(bundle!=null)
		{
			regid=bundle.getString("REGID");
			deviceid=bundle.getString("DEVICEID");
			firstName=bundle.getString("FIRSTNAME");
			lastName=bundle.getString("LASTNAME");
			email=bundle.getString("EMAIL");
			password=bundle.getString("PASSWORD");
			mobileNo=bundle.getString("MOBILE");
			referal=bundle.getString("REFERAL");
			picSelected=bundle.getBoolean("BOOLEAN");
			lat=bundle.getString("LATITUDE");
			longitude=bundle.getString("LONGITUDE");
			Utility.printLog("pic selected falg "+picSelected);

		}
		initializations();

	}

	void initializations()
	{
		verificationnumber_tv=(TextView)findViewById(R.id.verificationnumber_tv);
		verificationnumber_countrycode_tv=(TextView)findViewById(R.id.verificationnumber_countrycode_tv);
		resend_verificationcode_bt=(Button)findViewById(R.id.resend_verificationcode_bt);
		et_verificationcode=(EditText)findViewById(R.id.et_verificationcode);
		signup_back=(Button) findViewById(R.id.signup_back);
		verificationnumber_tv.setText(mobileNo);
		signup_payment_skip=(Button) findViewById(R.id.signup_payment_skip);
		sessionManager=new SessionManager(SignupVerificationActivity.this);

		et_verificationcode.setFocusableInTouchMode(true);
		et_verificationcode.setFocusable(true);
		et_verificationcode.requestFocus();
		signup_payment_skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (et_verificationcode.getText().toString().equals("")) {
					Utility.ShowAlert(getResources().getString(R.string.enter_verificationcode), SignupVerificationActivity.this);

				} else {
					Utility.ShowAlert(getResources().getString(R.string.enter_correct_verification), SignupVerificationActivity.this);

				}
			}
		});




		et_verificationcode.addTextChangedListener(new TextWatcher()
		{

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{

				Utility.printLog(" inside onTextChanged  :: " + count);


				count=s.length();

				Utility.printLog(" inside onTextChanged  s.length() :: " + count);


				if(count==5)
				{
					Utility.printLog("inside onTextChanged  length is 5  ::");

					new BackgroundForValid_VerificationCode().execute();

				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after)
			{

				Utility.printLog(" inside beforeTextChanged  :: " + count);

				count=s.length();

				Utility.printLog(" inside beforeTextChanged  s.length() :: " + count);

			}
			@Override
			public void afterTextChanged(Editable s)
			{

				Utility.printLog(" inside afterTextChanged   :: " + s);

				Utility.printLog(" inside afterTextChanged  s.length() :: " + s.length());

			}
		});



		resend_verificationcode_bt.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				new BackgroundForGettingVerificationCode().execute();

			}
		});

		signup_back.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				finish();

			}
		});

	}
	class BackgroundForGettingVerificationCode extends AsyncTask<String,Void,String>
	{
		VerificationCodeResponse verificationResponse;
		ProgressDialog dialogL;

		protected void onPreExecute()
		{
			super.onPreExecute();
			dialogL= Utility.GetProcessDialogNew(SignupVerificationActivity.this, getResources().getString(R.string.waitForVerification));
			dialogL.setCancelable(true);
			if (dialogL!=null)
			{
				dialogL.setCancelable(false);
				dialogL.show();
			}
		}
		@Override
		protected String doInBackground(String... params)
		{
			String url= VariableConstants.BASE_URL+"getVerificationCode";

			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();

			Map<String, String> kvPairs = new HashMap<String, String>();

			kvPairs.put("ent_mobile",verificationnumber_tv.getText().toString());

			Utility.printLog("The kvPair values in verification code is :: " + kvPairs);

			HttpResponse httpResponse = null;
			try
			{
				httpResponse = Utility.doPost(url, kvPairs);
			}
			catch (ClientProtocolException e1)
			{
				e1.printStackTrace();
				Utility.printLog("doPost Exception......." + e1);
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
				Utility.printLog("doPost Exception......." + e1);
			}

			String jsonResponse = null;
			if (httpResponse!=null)
			{

				try
				{
					jsonResponse = EntityUtils.toString(httpResponse.getEntity());
					Utility.printLog("getVerification code Response: " + jsonResponse);
				}
				catch (ParseException e)
				{
					e.printStackTrace();
					Utility.printLog("doPost Exception......." + e);
				}
				catch (IOException e)
				{
					e.printStackTrace();
					Utility.printLog("doPost Exception......." + e);
				}
			}
			try
			{

				if(jsonResponse!=null)
				{
					Gson gson = new Gson();
					verificationResponse=gson.fromJson(jsonResponse,VerificationCodeResponse.class);
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(getApplicationContext(),getResources().getString(R.string.requestTimeout), Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
			catch(Exception e)
			{
				Utility.ShowAlert(getResources().getString(R.string.server_error), SignupVerificationActivity.this);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			if(dialogL!=null)
			{
				dialogL.dismiss();
			}
			if(verificationResponse!=null)
			{
				if(verificationResponse.getErrFlag().equals("0"))
				{

					showAlert(verificationResponse.getErrMsg());

				}
				else
				{
					showAlert(verificationResponse.getErrMsg());
				}
			}
			else
			{
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignupVerificationActivity.this);
				// set title
				alertDialogBuilder.setTitle(getResources().getString(R.string.alert));
				// set dialog message
				alertDialogBuilder
						.setMessage(getResources().getString(R.string.server_error))
						.setCancelable(false)

						.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog,int id)
							{
								// if this button is clicked, just close
								// the dialog box and do nothing
								finish();
							}
						});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				// show it
				alertDialog.show();
			}
		}
	}

	class BackgroundForValid_VerificationCode extends AsyncTask<String,Void,String>
	{
		ValidVerificationCodeResponse response;
		ProgressDialog dialogL;

		protected void onPreExecute()
		{
			super.onPreExecute();
			dialogL= Utility.GetProcessDialogNew(SignupVerificationActivity.this, getResources().getString(R.string.waitForVerification));
			dialogL.setCancelable(true);
			if (dialogL!=null)
			{
				dialogL.setCancelable(false);
				dialogL.show();
			}
		}
		@Override
		protected String doInBackground(String... params)
		{
			String url= VariableConstants.BASE_URL+"verifyPhone";

			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();

			Map<String, String> kvPairs = new HashMap<String, String>();

			kvPairs.put("ent_phone",mobileNo);
			kvPairs.put("ent_code",et_verificationcode.getText().toString());


			Log.i("Tag","The kvPair values in Valid Verification Code Activity is :: " + kvPairs);

			HttpResponse httpResponse = null;
			try
			{
				httpResponse = Utility.doPost(url, kvPairs);
			}
			catch (ClientProtocolException e1)
			{
				e1.printStackTrace();
				Utility.printLog("doPost Exception......." + e1);
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
				Utility.printLog("doPost Exception......." + e1);
			}

			String jsonResponse = null;
			if (httpResponse!=null)
			{

				try
				{
					jsonResponse = EntityUtils.toString(httpResponse.getEntity());
					Utility.printLog("Check Valid verification Code Response: " + jsonResponse);
				}
				catch (ParseException e)
				{
					e.printStackTrace();
					Utility.printLog("doPost Exception......." + e);
				}
				catch (IOException e)
				{
					e.printStackTrace();
					Utility.printLog("doPost Exception......." + e);
				}
			}
			try
			{

				if(jsonResponse!=null)
				{
					Gson gson = new Gson();
					response=gson.fromJson(jsonResponse,ValidVerificationCodeResponse.class);
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(getApplicationContext(),getResources().getString(R.string.requestTimeout), Toast.LENGTH_SHORT).show();
						}
					});
				}


			}
			catch(Exception e)
			{
				Utility.ShowAlert(getResources().getString(R.string.server_error), SignupVerificationActivity.this);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);
			if(dialogL!=null)
			{
				dialogL.dismiss();
			}
			if(response!=null)
			{


				if(response.getErrFlag().equals("0"))
				{

					new BackgroundTaskSignUp().execute();


				}
				else
				{
					showAlert(response.getErrMsg());
					et_verificationcode.setText(null);
				}


			}
			else
			{

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignupVerificationActivity.this);

				// set title
				alertDialogBuilder.setTitle(getResources().getString(R.string.alert));

				// set dialog message
				alertDialogBuilder
						.setMessage(getResources().getString(R.string.server_error))
						.setCancelable(false)

						.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog,int id)
							{
								// if this button is clicked, just close
								// the dialog box and do nothing
								finish();
							}
						});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				// show it
				alertDialog.show();


			}
		}
	}



	private void showAlert(String message)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.alert));

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						//closing the application
						dialog.dismiss();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	private class BackgroundTaskSignUp extends AsyncTask<String, Void, String>
	{
		SignUpResponse response;
		String jsonResponse;
		ProgressDialog dialogL;
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
			dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SignupVerificationActivity.this,getResources().getString(R.string.signing_you_up));
			dialogL.setCancelable(true);
			if(dialogL!=null)
			{
				dialogL.setCancelable(false);
				dialogL.show();
			}
		}

		@Override
		protected String doInBackground(String... params)
		{
			Map<String, String> kvPairs = new HashMap<String, String>();

			String url= VariableConstants.BASE_URL+"slaveSignup";

			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();



			//if(access_token!=null)

			kvPairs.put("ent_first_name",firstName);
			kvPairs.put("ent_last_name",lastName);
			kvPairs.put("ent_email",email);
			kvPairs.put("ent_password",password);
			kvPairs.put("ent_mobile",mobileNo);
			//kvPairs.put("ent_city",current_city_name);
/*			kvPairs.put("ent_latitude",String.valueOf(currentLatitude));
			kvPairs.put("ent_longitude",String.valueOf(currentLongitude));*/
			if(lat!=null && longitude !=null)
			{
				kvPairs.put("ent_latitude",lat);
				kvPairs.put("ent_longitude",longitude);
			}
			else
			{
				kvPairs.put("ent_latitude","26.3544482");
				kvPairs.put("ent_longitude","49.7122909");
			}


			if(referal!=null)
				kvPairs.put("ent_referral_code",referal);
			kvPairs.put("ent_terms_cond","true");
			kvPairs.put("ent_pricing_cond", "true");

			kvPairs.put("ent_dev_id",deviceid);
			kvPairs.put("ent_push_token",regid);
			kvPairs.put("ent_device_type","2");
			kvPairs.put("ent_date_time",curenttime);
			HttpResponse httpResponse = null;
			try {
				httpResponse = com.threembed.utilities.Utility.doPost(url,kvPairs);
			} catch (ClientProtocolException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			if (httpResponse!=null) {

				try {
					jsonResponse = EntityUtils.toString(httpResponse.getEntity());
					Utility.printLog("SignUp Response: " + jsonResponse);
				} catch (ParseException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (jsonResponse!=null)
			{
				Gson gson = new Gson();
				response=gson.fromJson(jsonResponse, SignUpResponse.class);
			}else
			{
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						Toast.makeText(SignupVerificationActivity.this,getResources().getString(R.string.requestTimeout), Toast.LENGTH_SHORT).show();
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);


			if(response!=null)
			{
				if(response.getErrFlag().equals("0"))
				{
					if(response.getToken()!=null)
						sessionManager.storeSessionToken(response.getToken());
					sessionManager.storeRegistrationId(regid);
					sessionManager.storeLoginId(email);
					sessionManager.storeDeviceId(deviceid);
					sessionManager.setIsLogin(true);
					sessionManager.storeServerChannel(response.getServerChn());
					//session.storeLoginResponse(jsonResponse);
					//session.storeCarTypes(jsonResponse);
					sessionManager.storeChannelName(response.getChn());
					sessionManager.storeCouponCode(response.getCoupon());
					sessionManager.setPresenceChn(response.getPresenseChn());
			/*		VariableConstants.PUBUB_SUBSCRIBE_KEY=response.getSub();
					VariableConstants.PUBNUB_PUBLISH_KEY=response.getPub();*/
					VariableConstants.STRIPE_PUBLISH_KEY=response.getStipeKey();


					if(Utility.isNetworkAvailable(SignupVerificationActivity.this))
					{
						new BackGroundTaskForUploadImage().execute();
					}
					else
					{
						Utility.ShowAlert(getResources().getString(R.string.network_connection_fail), SignupVerificationActivity.this);
					}
				}
				else
				{
					Toast.makeText(SignupVerificationActivity.this,response.getErrMsg(), Toast.LENGTH_SHORT).show();

					if(dialogL!=null)
					{
						dialogL.dismiss();
					}
				}
			}
			else
			{
				Toast.makeText(SignupVerificationActivity.this,getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
				if(dialogL!=null)
				{
					dialogL.dismiss();
				}
			}
		}
	}

	private class  BackGroundTaskForUploadImage extends AsyncTask<String, Void, String>
	{
		private long chunkLength=1000*1024;
		private long totalBytesRead=0;
		private long bytesRemaining ;
		private long FILE_SIZE ;
		private String fileName;
		UploadImgeResponse response;
		private ProgressDialog dialogL;
		private List<NameValuePair> uploadNameValuePairList;
		File mFile;

		@Override
		protected String doInBackground(String... params)
		{

			FileInputStream fin = null;
			totalBytesRead = 0;
			bytesRemaining=0;

			//mFile=new File("/sdcard"+"/SneekPeek", "picture"+".jpg");
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state))
			{
				//mFile= new File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME);
				mFile=mFileTemp;
			}
			else
			{
				//mFile= new File(getFilesDir(),VariableConstants.TEMP_PHOTO_FILE_NAME);
				mFile=mFileTemp;
			}


			String temp=com.threembed.utilities.Utility.getCurrentDateTimeStringGMT();
			temp=new String(temp.trim().replace(" ", "20"));
			temp=new String(temp.trim().replace(":", "20"));
			temp=new String(temp.trim().replace("-", "20"));

			fileName="PA"+firstName+temp+mFile.getName();

			try
			{
				fin= new FileInputStream(mFile);
			}
			catch (FileNotFoundException e2)
			{
				e2.printStackTrace();
			}

			if(mFile.isFile() && mFile.length()>0)
			{
				FILE_SIZE = mFile.length();
			}




			while (totalBytesRead < FILE_SIZE )
			{
				try
				{
					bytesRemaining = FILE_SIZE-totalBytesRead;

					if ( bytesRemaining < chunkLength ) // Remaining Data Part is Smaller Than CHUNK_SIZE
					{
						chunkLength = bytesRemaining;
					}

					byte []chunk = null;
					chunk=new byte[(int) chunkLength];

					byte fileContent[] = new byte[(int) chunkLength];
					try
					{
						fin.read(fileContent,0,(int)chunkLength);
					}
					catch (FileNotFoundException e1)
					{
						e1.printStackTrace();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}

					System.arraycopy(fileContent, 0, chunk, 0, (int) chunkLength);
					//byte[] encoded = Base64.encodeBase64(chunk);
					byte[] encoded = Base64.encode(chunk, Base64.NO_WRAP);
					String encodedString = new String(encoded);

					Utility.printLog("Base 64: " + encodedString);

					String sessiontoken=sessionManager.getSessionToken();

					Utility utility=new Utility();
					String curenttime=utility.getCurrentGmtTime();

					if(!picSelected)
					{
						String [] uploadParameter={sessiontoken,sessionManager.getDeviceId(),"","","2","1","1",curenttime};
						uploadNameValuePairList=utility.getUploadParameter(uploadParameter);

					}
					else
					{
						String [] uploadParameter={sessiontoken,sessionManager.getDeviceId(),fileName,encodedString,"2","1","1",curenttime};
						uploadNameValuePairList=utility.getUploadParameter(uploadParameter);

					}





					totalBytesRead=totalBytesRead+chunkLength;

					String result = utility.makeHttpRequest(VariableConstants.BASE_URL+"uploadImage","POST",uploadNameValuePairList);


					if(result!=null)
					{
						Gson gson = new Gson();
						response=gson.fromJson(result, UploadImgeResponse.class);
					}

				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);

			if(dialogL!=null)
			{
				dialogL.dismiss();
			}

			if(response!=null)
			{
				if(response.getErrFlag().equals("0"))
				{
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.signup_success), Toast.LENGTH_SHORT).show();
					sessionManager.setIsLogin(true);
					//Move to map activity
					Intent intent=new Intent(SignupVerificationActivity.this,SignupPayment.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
				}
				else
				{
					//Utility.ShowAlert(response.getErrMsg(), SignupPayment.this);

					Toast.makeText(getApplicationContext(), getResources().getString(R.string.signup_success), Toast.LENGTH_SHORT).show();
					sessionManager.setIsLogin(true);
					//Move to map activity
					Intent intent=new Intent(SignupVerificationActivity.this,SignupPayment.class);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
				}
			}
			else
			{
				//Utility.ShowAlert(response.getErrMsg(), SignupPayment.this);

				Toast.makeText(getApplicationContext(), getResources().getString(R.string.signup_success), Toast.LENGTH_SHORT).show();
				sessionManager.setIsLogin(true);
				//Move to map activity
				Intent intent=new Intent(SignupVerificationActivity.this,SignupPayment.class);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
			}
		}
	}

}
