
package com.roadyo.passenger.main;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.LoginResponse;
import com.threembed.roadyo.R;
import com.threembed.utilities.AppLocationService;
import com.threembed.utilities.GpsListener;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;

public class SigninActivity extends Activity implements OnClickListener
{
	private static final String TAG = "SigninActivity";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private EditText username,password;
	private TextView forgot_password;
	private ImageButton back;
	private Button login;
	private RelativeLayout RL_Signin;
	private GoogleCloudMessaging gcm;
	private static LoginResponse response;
	private SessionManager session;
	private double currentLatitude,currentLongitude;
	private String current_city_name;
	private AppLocationService appLocationService;
	private String regid,strServerResponse,jsonErrorParsing;
	private Context context;
	private String deviceid,SENDER_ID = VariableConstants.PROJECT_ID;
	Location gpsLocation ;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		session=new SessionManager(SigninActivity.this);
		initialize();

		appLocationService = new AppLocationService(SigninActivity.this);

		gpsLocation	= appLocationService.getLocation(LocationManager.GPS_PROVIDER);

		if(gpsLocation != null)
		{
			currentLatitude = gpsLocation.getLatitude();
			currentLongitude = gpsLocation.getLongitude();
		}
		else
		{
			Location nwLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);

			if(nwLocation != null)
			{
				currentLatitude = nwLocation.getLatitude();
				currentLongitude = nwLocation.getLongitude();
			}
		}

		if(checkPlayServices())
		{
			if (gcm == null)
			{
				gcm=GoogleCloudMessaging.getInstance(SigninActivity.this);
			}

			SessionManager session=new SessionManager(SigninActivity.this);
			regid=session.getRegistrationId();

			if(regid==null)
			{
				new BackgroundForRegistrationId().execute();
			}
			else
			{
				deviceid=getDeviceId(context);
			}


		}
		else
		{
			Utility.printLog("No valid Google Play Services APK found.");
		}
	}



	private class BackgroundForRegistrationId extends AsyncTask<String, Void, String>
	{
		private ProgressDialog dialogL;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			dialogL=Utility.GetProcessDialog(SigninActivity.this);

			if (dialogL!=null) {
				dialogL.show();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			SessionManager session=new SessionManager(SigninActivity.this);

			try {

				deviceid=getDeviceId(SigninActivity.this);
				regid = gcm.register(SENDER_ID);

				session.storeRegistrationId(regid);
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialogL!=null) {
				dialogL.dismiss();
			}

			if(regid==null)
			{
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SigninActivity.this,5);

				// set title
				alertDialogBuilder.setTitle(getResources().getString(R.string.alert));

				// set dialog message
				alertDialogBuilder
						.setMessage(getResources().getString(R.string.slow_internet_connection))
						.setCancelable(false)
				/*.setPositiveButton("Refresh",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {

						}
					  })*/
						.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								//closing the application
								finish();
							}
						});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		}
	}


	private void initialize()
	{
		username=(EditText)findViewById(R.id.user_name);
		password=(EditText)findViewById(R.id.password);
		forgot_password=(TextView)findViewById(R.id.forgot_password);
		back=(ImageButton)findViewById(R.id.login_back_btn);
		login=(Button)findViewById(R.id.login_btn);
		RL_Signin=(RelativeLayout)findViewById(R.id.rl_signin);

		login.setOnClickListener(this);
		forgot_password.setOnClickListener(this);
		back.setOnClickListener(this);
		RL_Signin.setOnClickListener(this);
		username.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				Utility.printLog("setOnFocusChangeListener hasFocus= " + hasFocus);
				if (!hasFocus) {

					if (username.getText().toString().trim().isEmpty()) {
						username.setError(getResources().getString(R.string.email_empty));
					} else if (!validateEmail(username.getText().toString().trim())) {
						username.setError(getResources().getString(R.string.invalid_email));
					}


				}
			}
		});


		username.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable editable) {
				username.setError(null);
			}
		});


	}

	@Override
	public void onClick(View v)
	{
		if(v.getId()==R.id.login_back_btn)
		{
			/*Intent intent=new Intent(SigninActivity.this,SplashActivity.class);
			intent.putExtra("NO_ANIMATION",true);
			startActivity(intent);
			//overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
*/
			session.setSplahVideo(true);

			finish();
			overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		}
		if(v.getId()==R.id.rl_signin)
		{
			session.setSplahVideo(true);

			finish();
			overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		}
		if(v.getId()==R.id.login_btn)
		{
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(login.getWindowToken(),InputMethodManager.RESULT_UNCHANGED_SHOWN);

			if(validateFields()){

				if(Utility.isNetworkAvailable(SigninActivity.this))
				{
					if(validateEmail(username.getText().toString())){
						UserLogin();
					}
					else{
						showAlert(getResources().getString(R.string.invalid_email));

					}
				}
				else
				{
					showAlert(getResources().getString(R.string.network_connection_fail));
				}
			}
			return;
		}

		if(v.getId()==R.id.forgot_password)
		{
			Intent intent=new Intent(SigninActivity.this,ForgotPwdActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.anim_two, R.anim.anim_one);
			return;
		}
	}



	private void UserLogin()
	{
		final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SigninActivity.this,getResources().getString(R.string.loggingIn));
		dialogL.setCancelable(false);
		if (dialogL!=null)
		{
			dialogL.show();
		}

		RequestQueue volleyRequest = Volley.newRequestQueue(SigninActivity.this);
		StringRequest myReq = new StringRequest(Request.Method.POST,VariableConstants.BASE_URL+"slaveLogin",
				new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {

						if (dialogL!=null)
						{
							dialogL.cancel();
						}
						strServerResponse = response;
						getUserLoginInfo(dialogL);
					}
				}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				if (dialogL!=null)
				{
					dialogL.cancel();
				}
				Utility.printLog("on error for the login "+error);
				Toast.makeText(SigninActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
			}
		}){
			protected HashMap<String,String> getParams() throws com.android.volley.AuthFailureError
			{
				HashMap<String,String> kvPair = new HashMap<String, String>();

				Utility utility=new Utility();
				String curenttime=utility.getCurrentGmtTime();


				kvPair.put("ent_email",username.getText().toString());
				kvPair.put("ent_password",password.getText().toString());
				kvPair.put("ent_dev_id",deviceid);
				kvPair.put("ent_push_token",regid);
				kvPair.put("ent_latitude",""+currentLatitude);
				kvPair.put("ent_longitude",""+currentLongitude);
				kvPair.put("ent_device_type","2");
				kvPair.put("ent_date_time",curenttime);
				Utility.printLog("params to login "+kvPair);

				return kvPair;
			};
		};
		volleyRequest.add(myReq);
	}

	private void getUserLoginInfo(ProgressDialog dialogL)
	{
		dialogL.dismiss();



		Gson gson = new Gson();
		response = gson.fromJson(strServerResponse, LoginResponse.class);

		Utility.printLog("Login Response "+strServerResponse);

		if(response.getErrFlag().equals("0"))
		{
			VariableConstants.COUNTER=0;
			//success code
			if(session.getLoginId()!=null)
				if(!session.getLoginId().equals(username.getText().toString().trim()))
				{
					session.clearSession();
				}

			session.storeCurrencySymbol("$");

			//session.storeLoginResponse(strServerResponse);
			//session.storeCarTypes(strServerResponse);
			session.storeRegistrationId(regid);
			session.storeDeviceId(deviceid);
			session.storeSessionToken(response.getToken());
			session.storeDeviceId(deviceid);
			session.setIsLogin(true);
			session.storeLoginId(username.getText().toString());
			session.storeServerChannel(response.getServerChn());
			//store channel
			session.storeChannelName(response.getChn());
			session.storeCouponCode(response.getCoupon());
			session.setPresenceChn(response.getPresenseChn());
		/*	VariableConstants.PUBUB_SUBSCRIBE_KEY=response.getSub();
			VariableConstants.PUBNUB_PUBLISH_KEY=response.getPub();*/
			VariableConstants.STRIPE_PUBLISH_KEY=response.getStipeKey();
			//Move to map activity
			Intent intent=new Intent(SigninActivity.this,MainActivityDrawer.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);

			return;
		}

		else
		{
			Toast.makeText(getApplicationContext(),response.getErrMsg(),Toast.LENGTH_SHORT).show();
		}

	}

	private boolean checkPlayServices()
	{
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(resultCode != ConnectionResult.SUCCESS)
		{
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
			{
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			}
			else
			{
				finish();
			}
			return false;
		}
		return true;
	}


	//to get device id
	public  String getDeviceId(Context context)
	{
		/*TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();*/

		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();

	}

	private boolean validateFields() {
		if(username.getText().toString().isEmpty())
		{
			showAlert(getResources().getString(R.string.email_empty));
			return false;
		}

		if(password.getText().toString().isEmpty())
		{
			showAlert(getResources().getString(R.string.password_empty));
			return false;
		}
		return true;
	}


	private void showAlert(String message)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,5);

		// set title
		alertDialogBuilder.setTitle(getResources().getString(R.string.alert));

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)

				.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						//closing the application
						dialog.dismiss();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}





	@Override
	public void onBackPressed()
	{
		/*Intent intent=new Intent(SigninActivity.this,SplashActivity.class);
		intent.putExtra("NO_ANIMATION",true);
		startActivity(intent);*/
		session.setSplahVideo(true);

		finish();
		overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
	}


	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "8c41e9486e74492897473de501e087dbc6d9f391");
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		FlurryAgent.onEndSession(this);
	}



	public boolean validateEmail(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
		{
			isValid = true;
		}
		return isValid;
	}





}
